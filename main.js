const express = require("express");
const app = express();
const port = 3000;
app.use(express.json());

//Endpoint: http://localhost:3000/

app.get("/", (req, res) => {
  res.send("Hello World!");
});

/**
 * Arrow function to sum 2 numbers
 * @param {Number} a is the first number
 * @param {Number} b is the second number
 * @return {Number} the sum of a and b params
 *
 */

const add = (a, b) => {
  const sum = a + b;
  return sum;
};

//Endpoint: http://localhost:3000/add?a=value&b=value

app.post("/add", (req, res) => {
  const a = req.body.a;
  const b = req.body.b;
  const sum = add(a, b);
  res.send(sum.toString());
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
